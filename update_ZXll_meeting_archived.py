#!/usr/bin/env python

from indicomb import indicomb

indicomb(
    headerHTML="Exotic Dilepton Search",
    API_KEY="77514f84-29c7-4310-baa7-24c10c6e6bc3",
    SECRET_KEY="74b736f0-af9c-4b6c-baaa-d1e0fa505f3c",
    output="/eos/user/y/yangz/www/homepage/analysis/zxll/Exotics_dilepton_archived.html",
    startDate="2015-01-01",
    endDate="2020-05-31",
    includeList=["Exotic Dilepton Search"],
    categoryNumbers=[3285],
)
