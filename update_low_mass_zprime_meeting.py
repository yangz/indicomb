#!/usr/bin/env python

from indicomb import indicomb

indicomb(
    headerHTML="Low mass Z' (4mu) meetings",
    API_KEY="77514f84-29c7-4310-baa7-24c10c6e6bc3",
    SECRET_KEY="74b736f0-af9c-4b6c-baaa-d1e0fa505f3c",
    output="/eos/user/y/yangz/www/homepage/analysis/low_m_zprime/HLRS_low_mass_Zprime.html",
    startDate="2020-05-01",
    includeList=["Low mass Z' (4mu) meeting"],
    categoryNumbers=[12238],
)
